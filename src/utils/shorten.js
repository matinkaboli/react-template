const shorten = c => c.substring(0, 130) + '...';

export default shorten;
